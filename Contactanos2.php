<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/contactanos.css">
    <link rel="shortcut icon" href="imgs/Logo Circular Blanco.png">
    <link href="https://fonts.googleapis.com/css2?family=Annie+Use+Your+Telescope&display=swap" rel="stylesheet">
    <script defer src="js/app.js"></script>
    <script src="js/bootstrap.min.js "></script>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contactanos</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">

            <a class="navbar-brand logo" href="index.html">
                <img src="imgs/Logo - Letras Blancas.png" alt="" width="250" height="90">
            </a>

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
            <div class="collapse navbar-collapse col-4" id="navbarSupportedContent">
                <ul class="a navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item ">
                        <a class="nav-link " aria-current="page " href="index.html ">Inicio</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link " aria-current="page " href="Adopciones.php ">Mascotas</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link " aria-current="page " href="Centros.php">Centros</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link active" aria-current="page " href="Contactanos.html">Contactanos</a>
                    </li>
                </ul>

                <div class="d-grid gap-2 d-md-flex justify-content-md-end">

                    <a href="index.html" role="button" class="btn btn-danger">Cerrar Sesion</a>
                    <button class="btn btn-outline-light boton" type="button"><a href="index.html"></a> Perfil</button>
                </div>


            </div>
        </div>
    </nav>
     <div class="row row-cols-2 row-cols-md-1 g-4 columnas">
        <div class="col-md-6 offset-md-3">
            <div class="card text-center">
                <div class="card-body">
                    <h5 class="card-title">DATOS</h5>
                    <form action="" method="post" autocomplete="off">
                        <label for="">Nombre</label>
                        <input type="text" name="nombre">
                        <br><br>
                        <label for="">Correo </label>
                        <input type="text" name="correo">
                        <br><br>
                        <label for="">Telefono</label>
                        <input type="text" name="tel">
                        <br>
                        <div>
                        <br>
                        <input type="submit" value="Enviar" >
                        </div>
                       
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <?php
    if($_POST){

      extract($_POST);
       include("db.php");
      // $cla = md5($pass);
      date_default_timezone_set('America/Toronto');
      $d=rand(1,100);
      $usu = "gen".strval($d);
       $fec = date('Y-m-d h:i:s a', time());
       // $fec="";
      //echo $fec,''+$usu;
       
       $consulta2 = "INSERT INTO usuario VALUES('$usu','$nombre','$correo','12345','2')";
       //echo $consulta; */
        include("db.php");
        $consulta = "INSERT INTO form_adop (id_usuario,fecha_sol,correo,nombre,telefono)VALUES('$usu','$fec','$correo','$nombre','$tel')";

        $resultado=$conexion->query($consulta);



       if ($conexion->query($consulta2)) {
          //echo "Usuario Insertado";
          $consulta1 = "INSERT INTO form_adop (id_usuario,fecha_sol,correo,nombre,telefono)VALUES('$usu','$fec','$correo','$nombre','$tel')";
          if($conexion->query($consulta1)) {
          echo "<Script>alert('DATOS ENVIADOS $fec');</Script>";
          echo "<Script>location='index.html';</Script>";
          }
        }else{
         echo "<Script>alert('DATOS NO ENVIADOS');</Script>";

       }

    }
?>

</body>

</html>