

$(document).ready(function() {


    document.getElementById('contenedor-form-mascota').style.display='block';
    document.getElementById('contenedor-form-CA-origen').style.display='none';
    document.getElementById('contenedor-form-CA-destino').style.display='none';
    document.getElementById('boton-actualizar').style.display='none';
    document.getElementById('boton-mostrar').style.display='none';
    document.getElementById('boton-mostrar-mascotas').style.display='none';
    document.getElementById('boton-mostrar-centros').style.display='none';





    $('#boton-siguiente-mascota').attr('disabled', true);




    $('#boton-siguiente-CA-destino').attr('disabled', true);


    dataCD();
    console.log('SE EJECUTO EL JS');
    CargarTraslados();






});

var id_traslado;


function cambiarSiguiente(data) {
    if (data === 2){
        document.getElementById('contenedor-form-mascota').style.display='none';
        document.getElementById('contenedor-form-CA-destino').style.display='none';
        document.getElementById('contenedor-form-CA-origen').style.display='block';
    }else if (data === 3){

       validarDuplicado();

        document.getElementById('contenedor-form-CA-origen').style.display='none';
        document.getElementById('contenedor-form-mascota').style.display='none';
        document.getElementById('contenedor-form-CA-destino').style.display='block';
    }



}

function cambiarAtras(data) {
    if (data === 1){
        document.getElementById('contenedor-form-CA-destino').style.display='none';
        document.getElementById('contenedor-form-CA-origen').style.display='none';
        document.getElementById('contenedor-form-mascota').style.display='block';
    }else if (data === 2){

        document.getElementById('contenedor-form-mascota').style.display='none';
        document.getElementById('contenedor-form-CA-destino').style.display='none';
        document.getElementById('contenedor-form-CA-origen').style.display='block';
    }


}

function dataCD() {



    let datos = {
        "identificador": 0,
        "nombre": "Jhon"
    };


    $.ajax({
        url: "consulta.php",
        type: "POST",
        data: datos,
        dataType: "json",

        success:function (data) {

            CargarTraslados(data);

        }

    })

}

function CargarTraslados(datao) {

    $("#tabla-traslados > tbody").html("");

   let datos = {
        "identificador": 1,
        "nombre": "Jhon"
    };


    $.ajax({
        url: "consulta.php",
        type: "POST",
        data: datos,
        dataType: "json",

        success:function (data) {

            console.log('data',data);

            var cuerpo = '';
            for (var i=0; i<data.length; i++){

                cuerpo= "<tr>"+
                    "<td>" + data[i].ID_TRAS + "</td>"+
                    "<td>" + data[i].FECHA_TRAS + "</td>"+
                    "<td>" + data[i].NOMBRE_EMP + "</td>"+
                    "<td>" + data[i].NOM_ANIMAL + "</td>"+
                    "<td>" + data[i].NOMBRE_CA + "</td>"+
                    "<td>" + datao[i].NOMBRE_CA + "</td>"+

                    "<td>" + "<button type='button' class='seleccionar icon-edit btn btn-secondary' onclick='obtenerRegistros("+data[i].ID_TRAS+")' </button>" + "</td>"+
                    "<td>" + "<button type='button' class='seleccionar icon-trash btn btn-danger' onclick='eliminarTraslado("+data[i].ID_TRAS+")' </button>" + "</td>"+

                    "<tr>";
                $('#tabla-traslados tbody').append(cuerpo);
            }
            document.getElementById('boton-mostrar').style.display='none';
            $('#Entrada-buscar-traslado').val("");

        }

    })

}

function CargarMascotas() {

    $("#tabla-animales > tbody").html("");

    let datos = {
        "identificador": 2,
        "nombre": "Jhon"
    };

    $.ajax({
        url: "consulta.php",
        type: "POST",
        data: datos,
        dataType: "json",

        success:function (data) {

            console.log('data',data);

            var cuerpo = '';
            for (var i=0; i<data.length; i++){

                cuerpo= "<tr>"+
                    "<td>" + data[i].ID_ANIMAL + "</td>"+
                    "<td>" + data[i].NOM_ANIMAL + "</td>"+
                    "<td>" + data[i].NOMBRE + "</td>"+
                    "<td>" + data[i].RAZA + "</td>"+
                    "<td>" + data[i].EDAD + "</td>"+
                    "<td>" + data[i].SEXO + "</td>"+
                    "<td>" + data[i].NOMBRE_CA + "</td>"+


                    "<td>" + "<button type='button' class='seleccionar icon-check btn btn-primary' onclick='seleccionarMascota("+data[i].ID_ANIMAL+")' " +
                    " data-bs-toggle='modal' data-bs-target='#exampleModal' </button>" + "</td>"+


                    "<tr>";
                $('#tabla-animales tbody').append(cuerpo);
            }

            document.getElementById('boton-mostrar-mascotas').style.display='none';
            $('#Entrada-buscar-mascota').val("");
        }

    })

}


function seleccionarMascota(id) {


    $('#tabla-animales tbody tr').each(function (index) {

        var id_mascota ='';
        var nombre_mascota='';
        var sexo ='';
        var raza='';
        var edad='';
        var centro_mascota='';

        $(this).children("td").each(function (index2) {

            switch (index2) {
                case 0:
                    id_mascota = $(this).text();

                break;

                case 1:
                    nombre_mascota = $(this).text();
                break;

                case 3:
                    raza = $(this).text();
                    break;

                case 4:
                    edad = $(this).text();
                    break;

                case 5:
                    sexo = $(this).text();
                    break;

                case 6:
                    centro_mascota = $(this).text();
                    break;
            }

        } );

    if (id_mascota == id){



        $('#id-animal').val(id_mascota);
        $('#nombre-animal').val(nombre_mascota);
        $('#raza').val(raza);
        $('#edad').val(edad);
        $('#sexo').val(sexo);
        $('#nombre-centro-mascota').val(centro_mascota);
        buscarCentroo(id);
        $('#boton-siguiente-mascota').attr('disabled', false);

    }


    });
}


function CargarCentros() {

    var caDestinoSelecionado =  $('#id-centro-destino').val();

    $("#tabla-centros > tbody").html("");

    let datos = {
        "identificador": 3,
        "nombre": "Jhon"
    };

    $.ajax({
        url: "consulta.php",
        type: "POST",
        data: datos,
        dataType: "json",

        success:function (data) {

            console.log('data',data);

            var cuerpo = '';
            for (var i=0; i<data.length; i++){


            if (data[i].ID_CENTRO !== caDestinoSelecionado){

                cuerpo= "<tr>"+
                    "<td>" + data[i].ID_CENTRO + "</td>"+
                    "<td>" + data[i].NOMBRE_CA + "</td>"+
                    "<td>" + data[i].DIRECCION + "</td>"+
                    "<td>" + data[i].TELEFONO + "</td>"+
                    "<td>" + data[i].NOMBRE + "</td>"+
                    "<td>" + data[i].NOM_EMP + "</td>"+



                    "<td>" + "<button type='button' class='seleccionar icon-check btn btn-primary' onclick='seleccionarCentroOrigen("+data[i].ID_CENTRO+")' " +
                    " data-bs-toggle='modal' data-bs-target='#exampleModal' </button>" + "</td>"+


                    "<tr>";
                $('#tabla-centros tbody').append(cuerpo);
               }

            }

            document.getElementById('boton-mostrar-centros').style.display='none';
            $('#Entrada-buscar-centros').val("");

        }

    })

}



function seleccionarCentroOrigen(id) {


    var id_co ='';
    var nombre_co='';
    var direccion_co ='';
    var telefono_co='';
    var ciudad_co='';
    var empleado_co='';

    $('#tabla-centros tbody tr').each(function (index) {



        $(this).children("td").each(function (index2) {

            switch (index2) {
                case 0:
                    id_co = $(this).text();

                    break;

                case 1:
                    nombre_co = $(this).text();
                    break;

                case 2:
                    direccion_co = $(this).text();
                    break;

                case 3:
                    telefono_co = $(this).text();
                    break;

                case 4:
                    ciudad_co = $(this).text();
                    break;

                case 5:
                    empleado_co = $(this).text();
                    break;

            }

        } );

        if (_id_co == id){


            $('#id-centro-origen').val(id_co);
            $('#nombre-centro-origen').val(nombre_co);
            $('#direccion-centro-origen').val(telefono_co);
            $('#telefono-centro-origen').val(direccion_co);
            $('#ciudad-centro-origen').val(ciudad_co);
            $('#empleado-centro-origen').val(empleado_co);



        }


    });
}

function CargarCentrosDestino() {

    var caOrigenSelecionado =  $('#id-centro-origen').val();


    $("#tabla-centros > tbody").html("");

    let datos = {
        "identificador": 3,
        "nombre": "Jhon"
    };

    $.ajax({
        url: "consulta.php",
        type: "POST",
        data: datos,
        dataType: "json",

        success:function (data) {

            console.log('data',data);

            var cuerpo = '';
            for (var i=0; i<data.length; i++){

                if (data[i].ID_CENTRO !== caOrigenSelecionado){

                    cuerpo= "<tr>"+
                        "<td>" + data[i].ID_CENTRO + "</td>"+
                        "<td>" + data[i].NOMBRE_CA + "</td>"+
                        "<td>" + data[i].DIRECCION + "</td>"+
                        "<td>" + data[i].TELEFONO + "</td>"+
                        "<td>" + data[i].NOMBRE + "</td>"+
                        "<td>" + data[i].NOM_EMP + "</td>"+



                        "<td>" + "<button type='button' class='seleccionar icon-check btn btn-primary' onclick='seleccionarCentroDestino("+data[i].ID_CENTRO+")' " +
                        " data-bs-toggle='modal' data-bs-target='#exampleModal' </button>" + "</td>"+


                        "<tr>";
                    $('#tabla-centros tbody').append(cuerpo);
                }

            }

        }

    })

}


function seleccionarCentroDestino(id) {

    $('#tabla-centros tbody tr').each(function (index) {

        var id_cd ='';
        var nombre_cd='';
        var direccion_cd ='';
        var telefono_cd='';
        var ciudad_cd='';
        var empleado_cd='';

        $(this).children("td").each(function (index2) {

            switch (index2) {
                case 0:
                    id_cd = $(this).text();

                    break;

                case 1:
                    nombre_cd = $(this).text();
                    break;

                case 2:
                    direccion_cd = $(this).text();
                    break;

                case 3:
                    telefono_cd = $(this).text();
                    break;

                case 4:
                    ciudad_cd = $(this).text();
                    break;

                case 5:
                    empleado_cd = $(this).text();
                    break;

            }

        } );

        if (id_cd == id){


            $('#id-centro-destino').val(id_cd);
            $('#nombre-centro-destino').val(nombre_cd);
            $('#direccion-centro-destino').val(telefono_cd);
            $('#telefono-centro-destino').val(telefono_cd);
            $('#ciudad-centro-destino').val(ciudad_cd);
            $('#empleado-centro-destino').val(empleado_cd);

            $('#boton-siguiente-CA-destino').attr('disabled', false);

        }


    });
}




function crearTraslado() {

  var hoy = new Date();
  var date = hoy.getDate() + '-' + ( hoy.getMonth() + 1 ) + '-' + hoy.getFullYear();
  date = ""+date;

 var id_mascota = $('#id-animal').val();
 var id_centro_origen = $('#id-centro-origen').val();
 var id_centro_destino = $('#id-centro-destino').val();




    let datos = {
        "identificador": 4,
        "fecha": "2022-01-20",
        "usuario": "Adriana Caicedo",
        "mascota": id_mascota,
        "centro_origen":id_centro_origen,
        "centro_destino":id_centro_destino
    };
    setTimeout(function(){
        console.log("Hola Mundo");
        Swal.fire({
            title: 'Esta Seguro?',
            text: "Desea confirmar la creación de este nuevo registro!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, confirmar !'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: "consulta.php",
                    type: "POST",
                    data: datos,
                    dataType: "json",

                    success:function (data) {


                        dataCD();
                        reiniciar();

                    }

                })
                Swal.fire(
                    'Finalizado!',
                    'Creación exitosa.',
                    'success'
                )
            }
        })


    }, 1000);




}


function buscarCentroo(data) {

    let datos = {
        "identificador": 5,
        "id_mascota": data,

    };

    $.ajax({
        url: "consulta.php",
        type: "POST",
        data: datos,
        dataType: "json",

        success:function (data) {

          for (var i=0; i<data.length; i++){

               id_co= data[i].ID_CENTRO
               nombre_co = data[i].NOMBRE_CA
               direccion_co = data[i].DIRECCION
               telefono_co = data[i].TELEFONO
               ciudad_co = data[i].NOMBRE
               empleado_co = data[i].NOM_EMP
          }

            $('#id-centro-origen').val(id_co);
            $('#nombre-centro-origen').val(nombre_co);
            $('#direccion-centro-origen').val(telefono_co);
            $('#telefono-centro-origen').val(direccion_co);
            $('#ciudad-centro-origen').val(ciudad_co);
            $('#empleado-centro-origen').val(empleado_co);
            $('#nombre-centro-mascota').val(nombre_co);


        }

    })

}

function validarDuplicado() {


     var id_co = $('#id-centro-origen').val();
     var id_cd = $('#id-centro-destino').val();



    if (id_co == id_cd){

        $('#id-centro-destino').val("");
        $('#nombre-centro-destino').val("");
        $('#direccion-centro-destino').val("");
        $('#telefono-centro-destino').val("");
        $('#ciudad-centro-destino').val("");
        $('#empleado-centro-destino').val("");
        $('#boton-siguiente-CA-destino').attr('disabled', true);

    }
}




function obtenerRegistros(idTraslado) {

    id_traslado = idTraslado;

    $('#boton-siguiente-mascota').attr('disabled', false);

    let datos = {
        "identificador": 6,
        "id_traslado": idTraslado
    };


    $.ajax({
        url: "consulta.php",
        type: "POST",
        data: datos,
        dataType: "json",

        success:function (data) {

           console.log(data);
           var idMascota='';
            var cuerpo = '';
            for (var i=0; i<data.length; i++){

             //   OBTENIENDO INFORMACION DEL ANIMAL CON LA CONSULTA Y DEL CENTRO DE ORIGEN, CON EL METODO BUSCARCENTROO

                $('#id-animal').val(data[i].ID_ANIMAL);

                $('#nombre-animal').val(data[i].NOM_ANIMAL);
                $('#raza').val(data[i].RAZA);
                $('#edad').val(data[i].EDAD);
                $('#sexo').val(data[i].SEXO);



             //   CARGANDO INDORMACION DEL CENTRO DE DESTINO, CON LA CONSULTA

                $('#id-centro-destino').val(data[i].ID_CENTRO);
                $('#nombre-centro-destino').val(data[i].NOMBRE_CA);
                $('#direccion-centro-destino').val(data[i].DIRECCION);
                $('#telefono-centro-destino').val(data[i].TELEFONO);
                $('#ciudad-centro-destino').val(data[i].NOMBRE);
                $('#empleado-centro-destino').val(data[i].NOM_EMP);

                idMascota = data[i].ID_ANIMAL;
            }
            buscarCentroo(idMascota);
            $('#boton-siguiente-CA-destino').attr('disabled', false);
            document.getElementById('boton-actualizar').style.display='block';
            document.getElementById('boton-siguiente-CA-destino').style.display='none';



        }

    })

}

function actualizarTraslado() {


    var id_mascota = $('#id-animal').val();
    var id_centro_origen = $('#id-centro-origen').val();
    var id_centro_destino = $('#id-centro-destino').val();




    let datos = {
        "identificador": 7,
        "id_traslado": id_traslado,
        "id_mascota": id_mascota,
        "id_centro_origen":id_centro_origen,
        "id_centro_destino":id_centro_destino
    };

    setTimeout(function(){
        console.log("Hola Mundo");
        Swal.fire({
            title: 'Esta Seguro?',
            text: "Esta acción no se podrá revertir!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, actualizar!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: "consulta.php",
                    type: "POST",
                    data: datos,
                    dataType: "json",

                    success:function (data) {

                        dataCD();
                        reiniciar();

                    }

                })
                Swal.fire(
                    'Acturalizado!',
                    'El registro fue actualizado exitosamente.',
                    'success'
                )
            }
        })


    }, 1000);

}



function eliminarTraslado(id_traslado) {

    let datos = {
        "identificador": 8,
        "id_traslado": id_traslado,

    };

    setTimeout(function(){
        console.log("Hola Mundo");
        Swal.fire({
            title: 'Esta Seguro?',
            text: "Esta acción no se podrá revertir!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Eliminar!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: "consulta.php",
                    type: "POST",
                    data: datos,
                    dataType: "json",

                    success:function (data) {


                        dataCD();
                        reiniciar();

                    }

                })
                Swal.fire(
                    'Eliminado!',
                    'El registro fue eliminado exitosamente.',
                    'success'
                )
            }
        })


    }, 1000);

}


function trasladosBuscarAll() {

var id_traslado = $('#Entrada-buscar-traslado').val();

    let datos = {
        "identificador": 9,
        "id_traslado": id_traslado
    };


    $.ajax({
        url: "consulta.php",
        type: "POST",
        data: datos,
        dataType: "json",

        success:function (data) {

            trasladosBuscar(data,id_traslado);

        }

    })

}

function trasladosBuscar(datao, idTraslado) {

    $("#tabla-traslados > tbody").html("");


    let datos = {
        "identificador": 10,
        "id_traslado": idTraslado
    };


    $.ajax({
        url: "consulta.php",
        type: "POST",
        data: datos,
        dataType: "json",

        success:function (data) {

            console.log('data',data);

            var cuerpo = '';
            for (var i=0; i<data.length; i++){

                cuerpo= "<tr>"+
                    "<td>" + data[i].ID_TRAS + "</td>"+
                    "<td>" + data[i].FECHA_TRAS + "</td>"+
                    "<td>" + data[i].NOMBRE_EMP + "</td>"+
                    "<td>" + data[i].NOM_ANIMAL + "</td>"+
                    "<td>" + data[i].NOMBRE_CA + "</td>"+
                    "<td>" + datao[i].NOMBRE_CA + "</td>"+

                    "<td>" + "<button type='button' class='seleccionar icon-edit btn btn-secondary' onclick='obtenerRegistros("+data[i].ID_TRAS+")' </button>" + "</td>"+
                    "<td>" + "<button type='button' class='seleccionar icon-trash btn btn-danger' onclick='eliminarTraslado("+data[i].ID_TRAS+")' </button>" + "</td>"+

                    "<tr>";
                $('#tabla-traslados tbody').append(cuerpo);
            }
            document.getElementById('boton-mostrar').style.display='block';
        }

    })

}


function CargarMascotaConsulta() {

    var id_mascota = $('#Entrada-buscar-mascota').val();

    $("#tabla-animales > tbody").html("");

    let datos = {
        "identificador": 11,
        "id_mascota": id_mascota
    };

    $.ajax({
        url: "consulta.php",
        type: "POST",
        data: datos,
        dataType: "json",

        success:function (data) {

            console.log('data',data);

            var cuerpo = '';
            for (var i=0; i<data.length; i++){

                cuerpo= "<tr>"+
                    "<td>" + data[i].ID_ANIMAL + "</td>"+
                    "<td>" + data[i].NOM_ANIMAL + "</td>"+
                    "<td>" + data[i].NOMBRE + "</td>"+
                    "<td>" + data[i].RAZA + "</td>"+
                    "<td>" + data[i].EDAD + "</td>"+
                    "<td>" + data[i].SEXO + "</td>"+
                    "<td>" + data[i].NOMBRE_CA + "</td>"+


                    "<td>" + "<button type='button' class='seleccionar icon-check btn btn-primary' onclick='seleccionarMascota("+data[i].ID_ANIMAL+")' " +
                    " data-bs-toggle='modal' data-bs-target='#exampleModal' </button>" + "</td>"+


                    "<tr>";
                $('#tabla-animales tbody').append(cuerpo);
            }
            document.getElementById('boton-mostrar-mascotas').style.display='block';

        }

    })

}


function CargarCentroConsulta() {

    var id_centro = $('#Entrada-buscar-centros').val();

    $("#tabla-centros > tbody").html("");

    let datos = {
        "identificador": 12,
        "id_centro": id_centro
    };

    $.ajax({
        url: "consulta.php",
        type: "POST",
        data: datos,
        dataType: "json",

        success:function (data) {

            console.log('data',data);

            var cuerpo = '';
            for (var i=0; i<data.length; i++){

                    cuerpo= "<tr>"+
                        "<td>" + data[i].ID_CENTRO + "</td>"+
                        "<td>" + data[i].NOMBRE_CA + "</td>"+
                        "<td>" + data[i].DIRECCION + "</td>"+
                        "<td>" + data[i].TELEFONO + "</td>"+
                        "<td>" + data[i].NOMBRE + "</td>"+
                        "<td>" + data[i].NOM_EMP + "</td>"+



                        "<td>" + "<button type='button' class='seleccionar icon-check btn btn-primary' onclick='seleccionarCentroOrigen("+data[i].ID_CENTRO+")' " +
                        " data-bs-toggle='modal' data-bs-target='#exampleModal' </button>" + "</td>"+


                        "<tr>";
                    $('#tabla-centros tbody').append(cuerpo);


            }
            document.getElementById('boton-mostrar-centros').style.display='block';

        }

    })

}


function reiniciar() {

    $('#id-animal').val("");
    $('#nombre-animal').val("");
    $('#raza').val("");
    $('#edad').val("");
    $('#sexo').val("");
    $('#nombre-centro-mascota').val("");

    $('#boton-siguiente-mascota').attr('disabled', true);

    $('#id-centro-origen').val("");
    $('#nombre-centro-origen').val("");
    $('#direccion-centro-origen').val("");
    $('#telefono-centro-origen').val("");
    $('#ciudad-centro-origen').val("");
    $('#empleado-centro-origen').val("");



    $('#id-centro-destino').val("");
    $('#nombre-centro-destino').val("");
    $('#direccion-centro-destino').val("");
    $('#telefono-centro-destino').val("");
    $('#ciudad-centro-destino').val("");
    $('#empleado-centro-destino').val("");
    $('#boton-siguiente-CA-destino').attr('disabled', true);

    document.getElementById('contenedor-form-CA-destino').style.display='none';
    document.getElementById('contenedor-form-CA-origen').style.display='none';
    document.getElementById('contenedor-form-mascota').style.display='block';
    document.getElementById('boton-actualizar').style.display='none';
    document.getElementById('boton-siguiente-CA-destino').style.display='block';
}






