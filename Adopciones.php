<?php
include './RegistroPets/Controladores/Conexion.php';
$sql=$conectar->query("SELECT * FROM animal") or die ($conectar->error);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="shortcut icon" href="imgs/Logo Circular Blanco.png">
    <link href="https://fonts.googleapis.com/css2?family=Annie+Use+Your+Telescope&display=swap" rel="stylesheet">
    <script defer src="js/app.js"></script>
    <script src="js/bootstrap.min.js "></script>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mascotas</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">

            <a class="navbar-brand logo" href="index.html">
                <img src="imgs/Logo - Letras Blancas.png" alt="" width="250" height="90">
            </a>

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
            <div class="collapse navbar-collapse col-4" id="navbarSupportedContent">
                <ul class="a navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item ">
                        <a class="nav-link " aria-current="page " href="index.html ">Inicio</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link active" aria-current="page " href="Adopciones.html ">Mascotas</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link " aria-current="page " href="Centros.php">Centros</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link " aria-current="page " href="Contactanos.php">Contactanos</a>
                    </li>
                </ul>
                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                    <button class="btn btn-outline-light me-md-2 bton" type="button">Iniciar Sesion</button>
                    <button class="btn btn-outline-light boton" type="button">Registrarse</button>
                </div>
            </div>

        </div>
    </nav>


    <div class="row row-cols-2 row-cols-md-3 g-4 columnas ">
    <?php
$animales = $conectar->query("SELECT * FROM animal");


while ($fila= $animales->fetch_array()) {
   $Ientificacion = $fila[0];
   $NOMBRE_ca=$fila[1];
   $ID_ESP=$fila[2];
   $ID_CENTRO=$fila[3];
   $EDAD=$fila[4];
   $SEXO=$fila[5];
   $RAZA=$fila[6];
   $FECH_NAC=$fila[7];
   $FECHA_ING=$fila[8];
   $PESO=$fila[9];
   $FOTO_ANI=$fila[10];

?>
    <div class="col ">
            <div class="card ">
                <img src="data:image/jpeg;base64,<?php echo base64_encode( $FOTO_ANI )  ?> " class="card-img-top " alt="... ">
                <div class="card-body ">
              
                
                    <h5 class="card-title "> <?php  echo $NOMBRE_ca  ?> </h5>
                   
                    <p class="card-text "><?php  echo $RAZA .  " Tiene ".$EDAD ."años, nacio el  ".$FECH_NAC ." y pesa " .$PESO." KG " ?></p>
                </div>
            </div>
        </div>
   
     
        
        <?php

}
?> 
    </div>
    <br> 


</body>

</html