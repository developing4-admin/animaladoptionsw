<?php
include 'Controladores/Conexion.php';
$sql=$conectar->query("SELECT * FROM animal") or die ($conectar->error);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="shortcut icon" href="../imgs/Logo Circular Blanco.png"> <!-- Logo inicial -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css2?family=Annie+Use+Your+Telescope&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="estyls.css">
    
    <title>Registro de animales</title>
</head>
<body>
 


<form class="formingresar" action="" method="post" enctype="multipart/form-data">
    <fieldset>
    <legend>Ingresa los Datos</legend>
        <label>Identificacion <input type="text" name="ID_ANIMAL"><br></label>
        <label>Nombre <input type="text" name="NOM_ANIMAL"><br></label>
        <label>Especie <select name="ID_ESP" id="">
<?php
$especies = $conectar->query("SELECT * FROM tipo_especie");
while ($fila= $especies->fetch_array()) {
   $ID_ESP = $fila[0];
   $NOMBRE=$fila[1];
?>
<option value="<?php  echo $ID_ESP  ?>"> <?php  echo $NOMBRE  ?> </option>
<?php

}
?>   
</select>
</label>
<label>Centro <select name="ID_CENTRO" id="">
<?php
$especies = $conectar->query("SELECT * FROM centros");
while ($fila= $especies->fetch_array()) {
   $ID_centro = $fila[0];
   $NOMBRE_ca=$fila[1];
?>
<option value="<?php  echo $ID_centro  ?>"> <?php  echo $NOMBRE_ca  ?> </option>
<?php

}
?>
</select>
</label>
        <label>Edad <input type="text" name="EDAD"><br></label>
        <label>Sexo <input type="text" name="SEXO"><br></label>
        <label>Raza <input type="text" name="RAZA"><br></label>
        <label>Fecha Nacimiento <input type="date" name="FECH_NAC"><br></label>
        <label>Fecha de ingreso <input type="date" name="FECHA_ING"><br></label>
        <label>Peso <input type="text" name="PESO"><br></label>
        <label>Ingresar Imagen <input type="file" name="txtImg"  >
        </label>
        <label><input class="insertar" type="submit" onclick="mensaje()" name="botonInsertar" value="Insertar Datos"> </label>
       </label>
       <a href="./mostrrdatos.php" class="insertar">Visualizar Datos</a>
    </fieldset>
    </form>
   
</body>

<script src="ingresar.js"></script>


</html>
<?php
 /*
    * PROGRMAMA: AnimalAdoptionSW
    * OBJETIVO: Ingresar datos de los animales 
    * FECHA: 15-01-2022
    */
    
    include 'Controladores/Conexion.php';

    if ($_POST){

        $idanimal= $_POST['ID_ANIMAL'];
        $nomanimal= $_POST['NOM_ANIMAL'];
        $idespecie= $_POST['ID_ESP'];
        $idcentro= $_POST['ID_CENTRO'];
        $edad= $_POST['EDAD'];
        $sexo= $_POST['SEXO'];
        $raza= $_POST['RAZA'];
        $fechanac= $_POST['FECH_NAC'];
        $fechaing= $_POST['FECHA_ING'];
        $peso= $_POST['PESO'];
        $nombre=$_FILES['txtImg']['name'];
        $tmp=$_FILES['txtImg']['tmp_name'];
        //$folder= 'imgs';
        //mueve el archivo del folder temporal a la nueva ruta
      
       
        //move_uploaded_file($tmp,'../imgs/'.$nombre);

        //extraer los bytes del archivo
        $bytesArchivo = addsLashes(file_get_contents($tmp));

        
    $conectar->query("INSERT INTO animal(ID_ANIMAL,NOM_ANIMAL,ID_ESP,ID_CENTRO,EDAD,SEXO,RAZA,FECH_NAC,FECHA_ING,PESO,FOTO_ANI)VALUES('$idanimal','$nomanimal','$idespecie','$idcentro','$edad','$sexo','$raza','$fechanac','$fechaing','$peso','$bytesArchivo')");
      

   

    }

$conectar->close();

?>

