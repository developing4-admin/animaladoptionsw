<?php
class Operaciones extends Conexion{
    private $campos=[];
    private $valores=[];
    private $cadenaCampos=[];
    private $cadenaValores=[];
    private $consulta=[];
    public function capturar(){
        foreach ($_POST as $camposform => $valoresform){
            $this->campos[]=$camposform;
            $this->valores[]="'".$valoresform."'";
        }
    }
    public function convertir(){
        //implode convierte array in String, 
        //primer argumento separador, arreglo
        $this->cadenaCampos=implode(", ",$this->campos);
        $this->cadenaValores=implode(", ",$this->valores);
    }
    public function consultas($tabla){
        $this->consulta="INSERT into ".$tabla."(".$this->cadenaCampos.") values(".$this->cadenaValores.")";

    }
    public function ejecutar(){
        $this->conectar->query($this->consulta);
        echo"Dato Insertado";
    }
}