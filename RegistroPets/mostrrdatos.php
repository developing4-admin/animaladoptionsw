<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="../imgs/Logo Circular Blanco.png"> <!-- Logo inicial -->
    <script defer src="../js/app.js"></script>
    <link rel="stylesheet" href="estyls.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <title>Visualizar datos</title>
</head>
<body>
       
<?php
include 'Controladores/Conexion.php';
$consulta="SELECT * FROM animal";
$sql=$conectar->query("SELECT * FROM animal") or die ($conectar->error);
$DATOS= $sql->fetch_array();

?>

<body>
	<div class="container-fluid py-5" >
	
	</div>
	<div class="container">
		<div class="card">
			<div class="card-header bg-info">
				Animales 
			</div>
			<div class="card-body">
				<table id="tabla" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">

<thead>
<tr>
<th scope="col">Identificacion</th>
<th scope="col">Nombre</th>
<th scope="col">Especie</th>
<th scope="col">Centro</th>
<th scope="col" >Edad</th>
<th scope="col">Sexo</th>
<th scope="col">Raza</th>
<th scope="col">Fecha Nacimiento </th>
<th scope="col">Fecha de ingreso </th>
<th scope="col">Peso</th>
<th scope="col"> Imagen</th>

</tr>
</thead>
<tbody>
<?php

$sql=$conectar->query("SELECT * FROM animal") or die ($conectar->error);

while($DATOS= $sql->fetch_object()){
?>
<tr>
    <td><?php echo $DATOS->ID_ANIMAL  ?></td>
    <td><?php echo $DATOS->NOM_ANIMAL  ?></td>
    <td><?php echo $DATOS->ID_ESP ?></td>
    <td><?php echo $DATOS->ID_CENTRO ?></td>
    <td><?php echo $DATOS->EDAD  ?></td>
    <td><?php echo $DATOS->SEXO  ?></td>
    <td><?php echo $DATOS->RAZA?></td>
    <td><?php echo $DATOS->FECH_NAC?></td>
    <td><?php echo $DATOS->FECHA_ING  ?></td>
    <td><?php echo $DATOS->PESO ?></td>
    <td> <img width="20%" src="data:image/jpeg;base64, <?php echo base64_encode( $DATOS->FOTO_ANI )?>"/></td>

</tr>
<?php
}

?>
</tbody>
</table>

</div>
		</div>
	</div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script>

    <script>
		$(document).ready(function() {
			$("#tabla").DataTable();
		});
	</script>

</body>


</html>