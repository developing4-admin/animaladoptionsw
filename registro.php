<!DOCTYPE html>
<html lang="en">
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
 
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulario de Registro</title>
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="css/cabecera.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css">
    <link rel="stylesheet" href="css/cabecera.css">
    <link rel="shortcut icon" href="imgs/Logo Circular Blanco.png">
</head>
<body>
    <h1>Formulario de Registro</h1>
    <form action="" method="post">
        <table>
            <tr>
                <td>
                    Nombre y Apellido
                </td>
                <td>
                    <input type="name" name="realname">
                </td>
            </tr>
            <tr>
                <td>
                    Nick de Usuario
                </td>
                <td>
                    <input type="name" name="nick">
                </td>
            </tr>
            <tr>
            <tr>
                <td>
                    Correo
                </td>
                <td>
                    <input type="email" name="email">
                </td>
            </tr> 
            <tr>
                <td>
                    Contraseña
                </td>
                <td>
                    <input type="password" name="pass">
                </td>
            </tr>
            <tr>
                <td>
                   Repetir Contraseña
                </td>
                <td>
                    <input type="password" name="rpass">
                </td>
            </tr>
        </table>
        <input type="submit" name="submit" value="Registrarme"> <input type="reset" name="reset">
    </form>
    <?php
    if (isset($_POST['submit'])) 
       require("registro1.php")
    
    ?>
</body>
</html>