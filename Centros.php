<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="shortcut icon" href="imgs/Logo Circular Blanco.png">
    <link href="https://fonts.googleapis.com/css2?family=Annie+Use+Your+Telescope&display=swap" rel="stylesheet">
    <script defer src="js/app.js"></script>
    <script src="js/bootstrap.min.js "></script>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Centros</title>
</head>

<body id="centros">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid" >

            <a class="navbar-brand logo" href="index.html">
                <img src="imgs/Logo - Letras Blancas.png" alt="" width="250" height="90">
            </a>

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
            <div class="collapse navbar-collapse col-4" id="navbarSupportedContent">
                <ul class="a navbar-nav me-auto mb-2 mb-lg-0" >
                    <li class="nav-item ">
                        <a class="nav-link " aria-current="page " href="index.html ">Inicio</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link " aria-current="page " href="Adopciones.php">Mascotas</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link active" aria-current="page" >Centros</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link " aria-current="page " href="Contactanos.php">Contactanos</a>
                    </li>
                </ul>

                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                    <button class="btn btn-outline-light me-md-2 bton" type="button">Iniciar Sesion</button>
                    <button class="btn btn-outline-light boton" type="button">Registrarse</button>
                </div>
            </div>

        </div>
    </nav>

    <div class="row row-cols-2 row-cols-md-4 g-4 columnas" id="cols">
        <?php
              include("db.php");
              $consulta = "SELECT C.NOMBRE_CA NOMBRE, C.ID_CENTRO CENTRO, C.DIRECCION DIRECCION, C.TELEFONO TELEFONO, CI.NOMBRE CIUDAD FROM  CENTROS C INNER JOIN CIUDAD CI ON C.ID_CIUDAD = CI.ID_CIUDAD";
              $resultado = $conexion->query($consulta);
              $registro = $resultado -> fetch_array();
              do{
                echo "<div class='col'>";
                echo "<div class='card'>";
                echo "<div class='card-body'>" ;
                echo "<h5 class='card-title'>".$registro['NOMBRE']."</h5>";
                echo "<p class='card-text'>Codigo:".$registro['CENTRO']."</p>";
                echo "<p class='card-text'>Direccion:".$registro['DIRECCION']."</p>";
                echo "<p class='card-text'>Telefono:".$registro['TELEFONO']."</p>";
                echo "<p class='card-text'>Ciudad:".$registro['CIUDAD']."</p>";
                echo "</div>";
                echo "</div>";
                echo "</div>";    
              //  echo $registro['nombre']."<br>";
              //  echo $registro['Raza']."<br>";
              //  echo $registro['fecha']."<br>";
              //  echo $registro['ID']."<br>";
              //  echo "_____________________________";
    
            }while($registro=$resultado ->fetch_array())
              
       
        ?>
       <!-- <div class="col">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content.</p>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                </div>
            </div>
        </div>
    </div>
</body>
<br> Tiene que traer la informacion de los Centros de Adopcion de la DBA -->

</html>