
<?php
include_once('conexion.php');

$ident = $_POST['identificador'];

if ($ident == 0){
    $objeto = new Conexion();
    $conexion = $objeto->Conectar();
    $consulta = "SELECT * from traslado INNER JOIN animal ON traslado.ID_ANIMAL = animal.ID_ANIMAL INNER JOIN centros on traslado.ID_CENTRO_D = centros.ID_CENTRO";
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $data = $resultado->fetchAll(PDO::FETCH_ASSOC);

    print json_encode($data);

}elseif ($ident == 1){
    $objeto = new Conexion();
    $conexion = $objeto->Conectar();
    $consulta = "SELECT * from traslado INNER JOIN animal ON traslado.ID_ANIMAL = animal.ID_ANIMAL INNER JOIN centros on traslado.ID_CENTRO = centros.ID_CENTRO";
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $data = $resultado->fetchAll(PDO::FETCH_ASSOC);

    print json_encode($data);

} elseif ($ident == 2){

    $objeto = new Conexion();
    $conexion = $objeto->Conectar();
    $consulta = "SELECT * from animal INNER JOIN centros on animal.ID_CENTRO = centros.ID_CENTRO INNER JOIN tipo_especie on animal.ID_ESP = tipo_especie.ID_ESP";
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $data = $resultado->fetchAll(PDO::FETCH_ASSOC);

    print json_encode($data);

}elseif ($ident == 3){

    $objeto = new Conexion();
    $conexion = $objeto->Conectar();
    $consulta = "SELECT * from centros INNER JOIN ciudad on centros.ID_CIUDAD = ciudad.ID_CIUDAD";
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $data = $resultado->fetchAll(PDO::FETCH_ASSOC);
    print json_encode($data);

}elseif ($ident == 4){
    $respuesta ='';
    $fecha = $_POST['fecha'];
    $usuario = $_POST['usuario'];
    $mascota = $_POST['mascota'];
    $centro_origen = $_POST['centro_origen'];
    $centro_destino = $_POST['centro_destino'];

    $objeto = new Conexion();
    $conexion = $objeto->Conectar();
    $consulta = "INSERT INTO traslado (ID_TRAS, FECHA_TRAS, NOMBRE_EMP, ID_ANIMAL, ID_CENTRO, ID_CENTRO_D) VALUES (NULL, '$fecha', '$usuario', '$mascota', '$centro_origen','$centro_destino');";
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();

    if ($resultado){
        $respuesta=1;
        print json_encode($respuesta);
    }else{
        $respuesta=0;
        print json_encode($respuesta);
    }

} elseif ($ident == 5){

    $id_mascota = $_POST['id_mascota'];

    $objeto = new Conexion();
    $conexion = $objeto->Conectar();
    $consulta = "SELECT * from animal INNER JOIN centros on animal.ID_CENTRO = centros.ID_CENTRO INNER JOIN ciudad ON centros.ID_CIUDAD = ciudad.ID_CIUDAD WHERE animal.ID_ANIMAL='$id_mascota'";
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $data = $resultado->fetchAll(PDO::FETCH_ASSOC);
    print json_encode($data);

}elseif ($ident == 6){

    $id_traslado = $_POST['id_traslado'];

    $objeto = new Conexion();
    $conexion = $objeto->Conectar();
    $consulta = "SELECT * from traslado INNER JOIN animal ON traslado.ID_ANIMAL = animal.ID_ANIMAL INNER JOIN centros on traslado.ID_CENTRO_D = centros.ID_CENTRO  INNER JOIN ciudad on centros.ID_CIUDAD = ciudad.ID_CIUDAD  WHERE traslado.ID_TRAS ='$id_traslado'";
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $data = $resultado->fetchAll(PDO::FETCH_ASSOC);

    print json_encode($data);

}elseif ($ident == 7){

    $id_traslado = $_POST['id_traslado'];
    $id_animal = $_POST['id_mascota'];
    $id_centro_origen = $_POST['id_centro_origen'];
    $id_centro_destino = $_POST['id_centro_destino'];

    $objeto = new Conexion();
    $conexion = $objeto->Conectar();
    $consulta = "UPDATE traslado SET ID_ANIMAL='$id_animal', ID_CENTRO='$id_centro_origen', ID_CENTRO_D='$id_centro_destino' WHERE ID_TRAS ='$id_traslado'";
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    if ($resultado->rowCount()){
        $respuesta=1;
        print json_encode($respuesta);
    }else{
        $respuesta=0;
        print json_encode($respuesta);
    }

}elseif ($ident == 8){

    $id_traslado = $_POST['id_traslado'];

    $objeto = new Conexion();
    $conexion = $objeto->Conectar();
    $consulta = "DELETE FROM traslado WHERE traslado.ID_TRAS ='$id_traslado'";
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();

    if ($resultado->rowCount()){
        $respuesta=1;
        print json_encode($respuesta);
    }else{
        $respuesta=0;
        print json_encode($respuesta);
    }



}elseif ($ident == 9){

    $id_traslado = $_POST['id_traslado'];
    $objeto = new Conexion();
    $conexion = $objeto->Conectar();
    $consulta = "SELECT * from traslado INNER JOIN animal ON traslado.ID_ANIMAL = animal.ID_ANIMAL INNER JOIN centros on traslado.ID_CENTRO_D = centros.ID_CENTRO WHERE traslado.ID_TRAS='$id_traslado'";
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $data = $resultado->fetchAll(PDO::FETCH_ASSOC);

    print json_encode($data);

}elseif ($ident == 10){

    $id_traslado = $_POST['id_traslado'];
    $objeto = new Conexion();
    $conexion = $objeto->Conectar();
    $consulta = "SELECT * from traslado INNER JOIN animal ON traslado.ID_ANIMAL = animal.ID_ANIMAL INNER JOIN centros on traslado.ID_CENTRO = centros.ID_CENTRO WHERE traslado.ID_TRAS='$id_traslado'";
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $data = $resultado->fetchAll(PDO::FETCH_ASSOC);

    print json_encode($data);

}elseif ($ident == 11){

    $id_mascota = $_POST['id_mascota'];

    $objeto = new Conexion();
    $conexion = $objeto->Conectar();
    $consulta = "SELECT * from animal INNER JOIN centros on animal.ID_CENTRO = centros.ID_CENTRO INNER JOIN tipo_especie on animal.ID_ESP = tipo_especie.ID_ESP WHERE animal.ID_ANIMAL='$id_mascota'";
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $data = $resultado->fetchAll(PDO::FETCH_ASSOC);

    print json_encode($data);

}elseif ($ident == 12){

    $id_centro = $_POST['id_centro'];

    $objeto = new Conexion();
    $conexion = $objeto->Conectar();
    $consulta = "SELECT * from centros INNER JOIN ciudad on centros.ID_CIUDAD = ciudad.ID_CIUDAD WHERE centros.ID_CENTRO='$id_centro'";
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();
    $data = $resultado->fetchAll(PDO::FETCH_ASSOC);
    print json_encode($data);

}

